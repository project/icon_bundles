# Icon Bundles

This module allows you to define "bundles" of svg icons in a module or theme `*.icons.yml` file. These icons are available through an Icon Field (provided by this module) or a render array with the `icon` type.

Icons are embedded as an `svg` element directly into your HTML. Either with the full svg code using the `svg-files` embed method or a `use` element with `xlink:href` for the `svg-map` embed method (this method requires using `symbol` elements with `id` attributes in your svg sprite file).

**Optional submodules providing icon bundles:**
- Icon Bundles: Material Icons (`icon_bundles_material_icons`)
  - [Google's "material design" icons](https://google.github.io/material-design-icons) (v3.0.1)
  - Hundreds of icons across 16 categories/bundles
  - Icons grouped in svg sprite sheets with symbols
  - Small file size (each sprite sheet 1-63kb)
- Icon Bundles: Bootstrap Icons (`icon_bundles_bootstrap_icons`)
  - [Bootstrap's icons](https://icons.getbootstrap.com/) (v1.2.1)
  - About 1,200 icons (all in one bundle)
  - One ~630kb svg sprite sheet

**Optional submodules providing additional features:**
- Icon Bundles: Menu Icons (`icon_bundles_menu_icons`)
  - Adds an "Icon" field to menu link content forms
  - Icons from any icon bundle enabled site-wide will be available to choose from
  - The selected icon will be added as an `svg` element in the menu link just before the title
  - Some css is added to menu link icons as a starting point and can easily be overridden
  - No icons are in this module

## Installation

- Require through composer.
- Enable one of our submodules or create an `*.icons.yml` file in your module or theme to define your own icon bundles.
- Go to `/admin/config/content/icon_bundles/settings` to enable the bundles you'd like to use.
- Add an icon field to your content type (available bundles configurable) or use a render array in your code with `'#type' => 'icon'`

## Your `*.icons.yml` File

The `*` in your filename should match the name of your module or theme. Each "bundle" is described in your yaml file with an id. The id must be unique and **must not contain a period (".")**. For each bundle you provide:
- `label` (optional)
  - used in the settings form
  - if not provided a label will be generated from the bundle id
- `embed_method` (**required**)
  - either `svg-files` or `svg-map`
- `path` (**required** if using `svg-files` embed method)
  - relative path to the location of your module's/theme's icon files
  - icons will be found (recursively) and created from all svg files located here
  - each icon should have a unique filename within the bundle, even if they're in different folders
  - the provided svg icon will be directly embedded in your HTML 
- `map` (**required** if using `svg-map` embed method)
  - relative path to the location of the svg file used as a symbol sprite map
  - icons will be found through `id` attributes of `<symbol>`s in the file
  - `id` attributes are assumed to be unique within the file
  - the svg will be included in your HTML with an `xlink:href` attribute
- `exclude` (optional)
  - a list of filenames (if using `svg-files` embed method) or ids (if using `svg-map` embed method)
  - these will be excluded from the list of icons for this bundle
- `include` (optional)
  - a list of icons that will be included for this bundle (all others excluded)
  - provide the filenames or ids of the icons to be included (as in `exclude`)
  - **OR** provide a (unique) name for each icon as well as:
    - `label` (optional)
      - used as alt text for icon field labels in forms
      - if not provided a label will be generated from the svg filename/id
    - `filename` (**required** if using `svg-files` embed method)
      - where it's located in the `path` provided for the bundle
    - `id` (**required** if using `svg-map` embed method)
      - don't include the `#` part of the id
      
## Examples

Provide icon bundles through a yaml file:

```yaml
my_bundle:
  label: 'My Icons'
  embed_method: 'svg-files'
  path: 'icons/mine'
  exclude:
    - 'nested/bad_icon.svg'
    - 'my_icon.svg'
    
other_icons:
  embed_method: 'svg-map'
  map: 'icons/other.svg'
  include:
    icon:
      id: 'icon-id'
    thing:
      label: 'Thing Icon'
      id: 'thingy_icon-id'
```

Display an icon using a render array:

```php
$build['icon'] = [
  '#type' => 'icon',
  '#bundle' => 'other_icons',
  '#icon' => 'thing',
];
```
