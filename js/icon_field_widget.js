(function ($, Drupal, once) {

  var behaviors = Drupal.behaviors,
      debounce = Drupal.debounce;

  var iconBundlesIconsFiltered = false;

  behaviors.iconBundlesIconFilter = {
    attach: function (context) {
      var $container = $('.js-form-type-icon-field', context);
      var $filterIcons = $container.find('.js-form-type-radio');

      function filterIconList(e) {
        var query = $(e.target).val().toLowerCase();

        var toggleIconEntry = function toggleIconEntry(index, icon) {
          var $icon = $(icon);
          var textMatch = $icon.find('.icon').attr('aria-label').toLowerCase().indexOf(query) !== -1;
          $icon.toggle(textMatch);
        };

        if (query.length >= 1) {
          $filterIcons.each(toggleIconEntry);
          iconBundlesIconsFiltered = true;
        } else if (iconBundlesIconsFiltered) {
          iconBundlesIconsFiltered = false;
          $filterIcons.show();
        }
      }

      $(once('icon-filter-text', 'input.js-icon-bundles-filter', context)).on('keyup', debounce(filterIconList, 200));
    }
  };

  behaviors.iconBundlesCurrentIcon = {
    attach: function(context) {
      var $iconInputs = $('.icon-bundles-icons input[type="radio"]', context);
      var $currentIcon = $(once('icon-current', '.icon-bundles-icons input[type="radio"]:checked', context));

      $currentIcon.closest('.js-form-type-icon-field')
        .find('.icon-bundles-current-icon')
        .text($currentIcon.next('label').find('.icon').attr('aria-label'));

      $iconInputs.on('change', function() {
        if ($(this).is(':checked')) {
          $(this).closest('.js-form-type-icon-field')
            .find('.icon-bundles-current-icon')
            .text($(this).next('label').find('.icon').attr('aria-label'));
        }
      });
    }
  };

})(jQuery, Drupal, once);
