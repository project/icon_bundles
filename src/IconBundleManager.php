<?php

namespace Drupal\icon_bundles;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\Component\Annotation\Plugin\Discovery\AnnotationBridgeDecorator;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\icon_bundles\Annotation\IconBundle;

/**
 * Provides the default icon_bundle manager.
 */
class IconBundleManager extends DefaultPluginManager {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a new IconBundleManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to find directories to search.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler to find directories to search.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler) {
    parent::__construct('Plugin/IconBundle', $namespaces, $module_handler, IconBundleInterface::class, IconBundle::class);
    $this->themeHandler = $theme_handler;

    $this->setCacheBackend($cache_backend, 'icon_bundle');
    $this->alterInfo('icon_bundle');

  }

  /**
   * {@inheritdoc}
   */
  protected function providerExists($provider) {
    return $this->moduleHandler->moduleExists($provider) || $this->themeHandler->themeExists($provider);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $discovery = new YamlDiscoveryDecorator($discovery, 'icons', $this->moduleHandler->getModuleDirectories() + $this->themeHandler->getThemeDirectories());
      $discovery->addTranslatableProperty('label');
      $discovery = new AnnotationBridgeDecorator($discovery, $this->pluginDefinitionAnnotationName);
      $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
      $this->discovery = $discovery;
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    if (!$definition instanceof IconBundleDefinition) {
      throw new InvalidPluginDefinitionException($plugin_id, sprintf('The "%s" icon bundle definition must extend %s', $plugin_id, IconBundleDefinition::class));
    }

    if (empty($definition->get('embed_method')) || !in_array($definition->get('embed_method'), ['svg-files', 'svg-map'])) {
      throw new PluginException(sprintf('Icon bundle (%s) definition requires "embed_method" to be set as "svg-files" or "svg-map".', $plugin_id));
    }

    if ($definition->get('embed_method') == 'svg-files' && empty($definition->get('path'))) {
      throw new PluginException(sprintf('Icon bundle (%s) definition: "path" property is required with "svg-files" embed method.', $plugin_id));
    }

    if ($definition->get('embed_method') == 'svg-map' && empty($definition->get('map'))) {
      throw new PluginException(sprintf('Icon bundle (%s) definition: "map" property is required with "svg-map" embed method.', $plugin_id));
    }
  }

  /**
   * Check if plugin definition exists for given id.
   *
   * @param string $id
   *
   * @return bool
   */
  public function bundleExists($id) {
    return in_array($id, array_keys($this->getDefinitions()));
  }

  /**
   * Return ids of icon bundles that are enabled in module settings.
   *
   * @return string[]
   */
  public function getEnabledIconBundleIds() {
    $settings = \Drupal::config('icon_bundles.settings');
    if (!empty($settings)) {
      return array_filter(
        array_keys($settings->getRawData()),
        function ($bundle_id) use ($settings) {
          return $settings->get("$bundle_id.enabled") === 1 && $this->bundleExists($bundle_id);
        }
      );
    }
    else {
      return [];
    }
  }

}
