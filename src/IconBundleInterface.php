<?php

namespace Drupal\icon_bundles;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for the Icon bundle plugins.
 */
interface IconBundleInterface extends PluginInspectionInterface {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\icon_bundles\IconBundleDefinition
   */
  public function getPluginDefinition();

  /**
   * Returns the label.
   *
   * @return string
   *   Bundle label
   */
  public function getLabel();

  /**
   * Returns a list of icons.
   *
   * @return Icon[]
   *   List of bundle's icons
   */
  public function getIcons();

  /**
   * Returns a specific icon.
   *
   * @param string $name
   *   Name of icon.
   *
   * @return Icon
   *   An Icon object
   */
  public function getIcon(string $name);

}
