<?php

namespace Drupal\icon_bundles;

/**
 * Provides icon information.
 */
class Icon {

  /**
   * Icon bundle id.
   *
   * @var string
   */
  private $bundle;

  /**
   * Icon name.
   *
   * @var string
   */
  private $name;

  /**
   * Icon label.
   *
   * @var string
   */
  private $label;

  /**
   * Path to uri.
   *
   * @var string
   */
  private $uri;

  /**
   * Icon embed method.
   *
   * @var string
   */
  private $method;

  /**
   * Construct Icon object.
   */
  public function __construct($bundle_id, $name, $label, $uri, $method) {
    $this->bundle = $bundle_id;
    $this->name = $name;
    $this->label = $label;
    $this->uri = $uri;
    $this->method = $method;

    return $this;
  }

  /**
   * Get icon bundle.
   *
   * @return IconBundleInterface|null
   */
  public function getBundle() {
    return \Drupal::service('plugin.manager.icon_bundle')->createInstance($this->bundle);
  }

  /**
   * Get icon name.
   *
   * @return string
   *   Icon name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get icon label.
   *
   * @return string
   *   Icon label
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Get icon uri.
   *
   * @return string
   *   Icon uri
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Get icon embed method.
   *
   * @return string
   *   Icon embed method 'svg-files' or 'svg-map'
   */
  public function getMethod() {
    return $this->method;
  }

}
