<?php

namespace Drupal\icon_bundles;

use Drupal\Component\Plugin\Definition\PluginDefinitionInterface;
use Drupal\Component\Plugin\Definition\PluginDefinition;

/**
 * Provides an implementation of an icon bundle definition and its metadata.
 */
class IconBundleDefinition extends PluginDefinition implements PluginDefinitionInterface {

  /**
   * The path to the bundle provider.
   *
   * @var string
   */
  protected $provider_path;

  /**
   * The human-readable name.
   *
   * @var string|null
   */
  protected $label;

  /**
   * Icon embed method 'svg-files' or 'svg-map'.
   *
   * @var string
   */
  protected $embed_method;

  /**
   * The path to svg files.
   *
   * @var string|null
   */
  protected $path;

  /**
   * The location of the svg map file.
   *
   * @var string|null
   */
  protected $map;

  /**
   * A list of icons (filenames or ids) to be included.
   *
   * @var string[]
   */
  protected $include;

  /**
   * A list of icons (filenames or ids) to be excluded.
   *
   * @var string[]
   */
  protected $exclude;

  /**
   * Bundle icons.
   *
   * @var \Drupal\icon_bundles\Icon[]
   */
  protected $icons;

  /**
   * IconBundleDefinition constructor.
   *
   * @param array $definition
   *   An array of values from the annotation.
   */
  public function __construct(array $definition) {
    $provider = $definition['provider'];
    /** @var \Drupal\Core\Extension\Extension $extension */
    if (\Drupal::service('module_handler')->moduleExists($provider)) {
      $extension = \Drupal::service('module_handler')->getModule($provider);
    }
    elseif (\Drupal::service('theme_handler')->themeExists($provider)) {
      $extension = \Drupal::service('theme_handler')->getTheme($provider);
    }
    $definition['provider_path'] = isset($extension) ? $extension->getPath() : '';

    foreach ($definition as $property => $value) {
      $this->set($property, $value);
    }

    $this->buildIcons();
  }

  /**
   * Gets any arbitrary property.
   *
   * @param string $property
   *   The property to retrieve.
   *
   * @return mixed
   *   The value for that property, or NULL if the property does not exist.
   */
  public function get($property) {
    $value = NULL;
    if (property_exists($this, $property)) {
      $value = isset($this->{$property}) ? $this->{$property} : NULL;
    }
    return $value;
  }

  /**
   * Sets a value to an arbitrary property.
   *
   * @param string $property
   *   The property to use for the value.
   * @param mixed $value
   *   The value to set.
   *
   * @return $this
   */
  public function set($property, $value) {
    if (property_exists($this, $property)) {
      $this->{$property} = $value;
    }
    return $this;
  }

  /**
   * Gets included icons.
   *
   * @return string[]
   */
  public function getIncluded() {
    return $this->include ?: [];
  }

  /**
   * Gets excluded icons.
   *
   * @return string[]
   */
  public function getExcluded() {
    return $this->exclude ?: [];
  }

  /**
   * Get icons.
   *
   * @return Icon[]
   */
  public function getIcons() {
    return $this->icons;
  }

  /**
   * Set icons.
   */
  private function buildIcons() {
    $included = $this->getIncluded();
    $embed_method = $this->get('embed_method');
    $provider_path = $this->get('provider_path');
    $icons = [];
    if (count($included)) {
      // Construct Icons from those provided in yaml file.
      foreach ($included as $name => $icon) {
        if (is_array($icon)) {
          $label = isset($icon['label'])
            ? $icon['label']
            : $this->createIconLabel($name);
          $uri = $embed_method == 'svg-files'
            ? $this->get('path') . '/' . $icon['filename']
            : $this->get('map') . '#' . $icon['id'];
        }
        else {
          $name = str_replace('.svg', '', $icon);
          $label = $this->createIconLabel($name);
          $uri = $embed_method == 'svg-files'
            ? $this->get('path') . '/' . $icon
            : $this->get('map') . '#' . $icon;
        }
        $icons[$name] = new Icon($this->id(), $name, $label, $provider_path . '/' . $uri, $embed_method);
      }
    }
    else {
      $excluded = $this->getExcluded();
      if ($embed_method == 'svg-map') {
        // Construct Icons from map ids.
        $map_uri = $this->get('map');
        $full_uri = $provider_path . '/' . $map_uri;
        $map_contents = file_get_contents($full_uri);
        preg_match_all(
          '/id="(?P<id>(\S+))"/',
          $map_contents,
          $map_ids
        );
        foreach ($map_ids['id'] as $id) {
          if (!in_array($id, $excluded)) {
            $label = $this->createIconLabel($id);
            $icons[$id] = new Icon($this->id(), $id, $label, $full_uri . '#' . $id, $embed_method);
          }
        }
      }
      elseif ($embed_method == 'svg-files') {
        // Construct Icons from files in path.
        $files_path = $provider_path . '/' . $this->get('path');
        // Add file path to excluded icon files for uri comparison.
        $excluded = array_map(function ($file) use ($files_path) {
          return $files_path . '/' . $file;
        }, $excluded);
        try {
          $files_list = \Drupal::service('file_system')->scanDirectory($files_path, '/.+\.svg$/');
          foreach ($files_list as $file) {
            if (!in_array($file->uri, $excluded)) {
              $label = $this->createIconLabel($file->name);
              $icons[$file->name] = new Icon($this->id(), $file->name, $label, $file->uri, $embed_method);
            }
          }
        }
        catch (\Exception $e) {
          \Drupal::logger('icon_bundles')->warning($e->getMessage());
          \Drupal::messenger()->addError($e->getMessage());
        }
      }
    }
    $this->icons = $icons;
  }

  /**
   * Create label for icon from given string.
   *
   * @param string $name
   *   Icon machine name.
   *
   * @return string
   *   More human-friendly label.
   */
  private function createIconLabel($name) {
    return ucwords(str_replace(['_', '/'], [' ', ' - '], $name));
  }

}
