<?php

/**
 * @file
 * Contains \Drupal\icon_bundles\Element\IconField.
 */

namespace Drupal\icon_bundles\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form element for icon_field.
 *
 * @FormElement("icon_field")
 */
class IconField extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processIconField'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Expands an icon field into needed components.
   */
  public static function processIconField(&$element, FormStateInterface $form_state, &$complete_form) {
    $options = self::getIconOptions($element['#bundles']);
    $element += [
      '#attached' => [
        'library' => ['icon_bundles/icon_widget'],
      ],
      'current' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => '',
        '#attributes' => [
          'class' => ['icon-bundles-current-icon'],
        ],
      ],
      'filter' => [
        '#type' => 'search',
        '#title' => t('Filter by icon name'),
        '#title_display' => 'invisible',
        '#size' => 30,
        '#placeholder' => t('Filter by icon name'),
        '#attributes' => [
          'class' => ['js-icon-bundles-filter'],
          'title' => t('Enter a part of the icon name to filter by.'),
        ],
      ],
      'icon' => count($options)
      ? [
        '#type' => 'radios',
        '#title' => t('Choose icon'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => $element['#default_value'],
        '#attributes' => [
          'class' => ['icon-bundles-icons'],
        ],
      ]
      : [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('No icons available.'),
      ],
    ];

    return $element;
  }

  /**
   * Returns icon options for the provided icon bundles.
   *
   * @param string[] $icon_bundles
   *   Array of bundle ids.
   *
   * @return array
   *   Array of icons.
   */
  public static function getIconOptions(array $icon_bundles = []) {
    $options = [];
    $enabled_bundles = \Drupal::service('plugin.manager.icon_bundle')->getEnabledIconBundleIds();
    foreach (array_filter($icon_bundles) as $id) {
      if (in_array($id, $enabled_bundles)) {
        /** @var \Drupal\icon_bundles\IconBundleInterface $bundle */
        $bundle = \Drupal::service('plugin.manager.icon_bundle')->createInstance($id);
        // TODO: group options by bundle if/when possible.
        // Issue https://www.drupal.org/project/drupal/issues/2269823
        $icons = $bundle->getIcons();
        foreach ($icons as $icon) {
          $title = [
            '#type' => 'icon',
            '#bundle' => $id,
            '#icon' => $icon->getName(),
            '#alt' => $icon->getLabel(),
          ];
          $options[$id . '::' . $icon->getName()] = \Drupal::service('renderer')->render($title);
        }
      }
    }
    return $options;
  }

}
