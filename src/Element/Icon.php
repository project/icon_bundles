<?php

namespace Drupal\icon_bundles\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides an icon render element.
 *
 * Properties:
 * - #bundle: The icon bundle.
 * - #icon: The name of the icon to display.
 * - #alt: Optional alt text. (Usually best empty.)
 *
 * Usage example:
 *
 * @code
 * $build['example_icon'] = [
 *   '#type' => 'icon',
 *   '#bundle' => 'my_icons',
 *   '#icon' => 'random-icon',
 *   '#alt' => 'Icon description',
 * ];
 * @endcode
 *
 * @RenderElement("icon")
 */
class Icon extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'icon',
      '#pre_render' => [
        [$class, 'preRenderIcon'],
      ],
    ];
  }

  /**
   * Pre-render callback: Derives icon uri and embed method.
   *
   * @param array $element
   *   - #bundle: The icon bundle.
   *   - #icon: The name of the icon to display.
   *
   * @return array
   *   The passed-in element containing a '#uri' and embed '#method'.
   */
  public static function preRenderIcon(array $element) {
    if (!empty($element['#bundle']) && !empty($element['#icon'])) {
      if (\Drupal::service('plugin.manager.icon_bundle')->bundleExists($element['#bundle'])) {
        /** @var \Drupal\icon_bundles\IconBundleInterface $bundle */
        $bundle = \Drupal::service('plugin.manager.icon_bundle')->createInstance($element['#bundle']);
        if ($bundle) {
          $icon = $bundle->getIcon($element['#icon']);
          if ($icon) {
            $element['#method'] = $bundle->getPluginDefinition()->get('embed_method');
            $element['#uri'] = $icon ? '/' . $icon->getUri() : NULL;
          }
          else {
            $element = [
              '#markup' => '<small hidden="true">Broken icon (icon not found in bundle)</small>',
            ];
          }
        }
      }
      else {
        $element = [
          '#markup' => '<small hidden="true">Broken icon (missing icon bundle)</small>',
        ];
      }
    }

    return $element;
  }

}
