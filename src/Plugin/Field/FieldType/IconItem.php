<?php

namespace Drupal\icon_bundles\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin Implementation of the 'icon_field' field type.
 *
 * @FieldType(
 *  id = "icon",
 *  label = @Translation("Icon"),
 *  module = "icon_bundles",
 *  description = @Translation("Store a bundle and icon in the database to assemble an icon field."),
 *  category = @Translation("General"),
 *  default_widget = "icon_widget",
 *  default_formatter = "icon_formatter"
 * )
 */
class IconItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'bundle' => [
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE,
        ],
        'icon' => [
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['bundle'] = DataDefinition::create('string')
      ->setLabel(t('Icon Bundle'))
      ->setDescription(t('Plugin id of the icon bundle.'));

    $properties['icon'] = DataDefinition::create('string')
      ->setLabel(t('Icon Name'))
      ->setDescription(t('The name of the icon.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'bundles' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $field = $form_state->getFormObject()->getEntity();

    $settings = \Drupal::config('icon_bundles.settings');
    $enabled_bundles = \Drupal::service('plugin.manager.icon_bundle')->getEnabledIconBundleIds();
    $bundle_options = [];
    foreach ($enabled_bundles as $id) {
      if ($settings->get($id) && $settings->get("$id.enabled")) {
        /** @var \Drupal\icon_bundles\IconBundleInterface $bundle */
        $bundle = \Drupal::service('plugin.manager.icon_bundle')->createInstance($id);
        $bundle_options[$id] = $bundle->getLabel();
      }
    }
    $form = [
      '#type' => 'container',
      'bundles' => [
        '#type' => 'checkboxes',
        '#title' => t('Icon bundles'),
        '#options' => $bundle_options,
        '#default_value' => $form_state->getValue('bundles') ?: $field->getSetting('bundles'),
        '#required' => TRUE,
        '#ajax' => [
          'callback' => [get_called_class(), 'settingsAjax'],
          'wrapper' => 'edit-default-value-input',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback for the settings form to update default value field.
   */
  public static function settingsAjax($form, FormStateInterface $form_state) {
    $form['default_value']['#id'] = 'edit-default-value-input';

    return $form['default_value'];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $bundle = $this->get('bundle')->getValue();
    $icon = $this->get('icon')->getValue();

    return empty($bundle) || empty($icon);
  }

}
