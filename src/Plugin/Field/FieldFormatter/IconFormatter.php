<?php

namespace Drupal\icon_bundles\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of 'icon' formatter.
 *
 * @FieldFormatter(
 *  id = "icon_formatter",
 *  label = @Translation("Icon"),
 *  field_types = {
 *    "icon"
 *  }
 * )
 */
class IconFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays the icon.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'icon',
        '#bundle' => $item->bundle,
        '#icon' => $item->icon,
      ];
    }

    return $elements;
  }

}
