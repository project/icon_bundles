<?php

namespace Drupal\icon_bundles\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of 'icon' widget.
 *
 * @FieldWidget(
 *  id = "icon_widget",
 *  label = @Translation("Icon"),
 *  module = "icon_bundles",
 *  field_types = {
 *    "icon"
 *  }
 * )
 */
class IconWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $icon_bundles = $form_state->getValue(['settings', 'bundles']) ?: $this->fieldDefinition->getSetting('bundles');
    $item = $items[$delta];
    $default = (!empty($item->get('bundle')->getValue()) && !empty($item->get('icon')->getValue()))
      ? $item->get('bundle')->getValue() . '::' . $item->get('icon')->getValue()
      : NULL;

    $element += [
      '#type' => 'icon_field',
      '#bundles' => $icon_bundles,
      '#default_value' => $default,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $return = [];
    foreach ($values as $value) {
      if (isset($value['icon']) && !empty($value['icon'])) {
        $icon = explode('::', $value['icon']);
        $return[] = ['bundle' => $icon[0], 'icon' => $icon[1]];
      }
    }
    return $return;
  }

}
