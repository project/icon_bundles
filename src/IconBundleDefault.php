<?php

namespace Drupal\icon_bundles;

use Drupal\Component\Plugin\PluginBase;

/**
 * Provides a default class for Icon Bundle plugins.
 */
class IconBundleDefault extends PluginBase implements IconBundleInterface {

  /**
   * The icon bundle definition.
   *
   * @var \Drupal\icon_bundles\IconBundleDefinition
   */
  protected $pluginDefinition;

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\icon_bundles\IconBundleDefinition
   */
  public function getPluginDefinition() {
    return $this->pluginDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->getPluginDefinition()->get('label') ?: ucwords(str_replace(['_', '.'], [' ', ' - '], $this->getPluginId()));
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons() {
    return $this->getPluginDefinition()->getIcons();
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon(string $name) {
    $icons = $this->getPluginDefinition()->getIcons();
    return isset($icons[$name]) ? $icons[$name] : NULL;
  }

}
