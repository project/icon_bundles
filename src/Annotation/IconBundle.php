<?php

namespace Drupal\icon_bundles\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\icon_bundles\IconBundleDefault;
use Drupal\icon_bundles\IconBundleDefinition;

/**
 * Defines an Icon Bundle annotation object.
 *
 * Icon Bundles are used to define a list of svg icons
 * to be used in icon fields or render arrays.
 *
 * Plugin namespace: Plugin\IconBundle
 *
 * @see \Drupal\icon_bundles\IconBundleInterface
 * @see \Drupal\icon_bundles\IconBundleDefault
 * @see \Drupal\icon_bundles\IconBundleManager
 * @see plugin_api
 *
 * @Annotation
 */
class IconBundle extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name.
   *
   * @var string
   */
  public $label;

  /**
   * Icon embed method 'svg-files' or 'svg-map'.
   *
   * @var string
   */
  public $embed_method;

  /**
   * The path to svg files.
   *
   * @var string|null
   */
  public $path;

  /**
   * The location of the svg map file.
   *
   * @var string|null
   */
  public $map;

  /**
   * A list of icons (filenames or ids) to be included.
   *
   * @var string[]
   */
  public $included;

  /**
   * A list of icons (filenames or ids) to be excluded.
   *
   * @var string[]
   */
  protected $excluded;

  /**
   * The icon bundle plugin class.
   *
   * This default value is used for plugins defined in icons.yml that do not
   * specify a class themselves.
   *
   * @var string
   */
  public $class = IconBundleDefault::class;

  /**
   * {@inheritdoc}
   */
  public function get() {
    return new IconBundleDefinition($this->definition);
  }

}
