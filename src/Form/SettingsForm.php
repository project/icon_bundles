<?php

namespace Drupal\icon_bundles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure icon bundle settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icon_bundles_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'icon_bundles.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('icon_bundles.settings');
    /** @var \Drupal\icon_bundles\IconBundleDefinition[] $bundle_definitions */
    $bundle_definitions = \Drupal::service('plugin.manager.icon_bundle')->getDefinitions();

    if (empty($bundle_definitions)) {
      $this->messenger()->addError($this->t('No icon bundles found.'));
      $form['info'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("Please enable a submodule or provide an <code>*.icons.yml</code> file in your module or theme, then clear the site's caches and return to this page to continue setup."),
      ];
      return $form;
    }

    $form['bundles'] = [
      '#type' => 'container',
      '#tree' => 'true',
      '#attached' => [
        'library' => ['icon_bundles/icon_list'],
      ],
    ];
    foreach ($bundle_definitions as $bundle_definition) {
      $id = $bundle_definition->id();
      /** @var \Drupal\icon_bundles\IconBundleInterface $bundle */
      $bundle = \Drupal::service('plugin.manager.icon_bundle')->createInstance($id);
      $form['bundles'][$id] = [
        '#type' => 'container',
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $bundle->getLabel(),
        ],
        'provider' => [
          '#type' => 'html_tag',
          '#tag' => 'small',
          '#value' => $this->t('Provider: %p', ['%p' => $bundle_definition->getProvider()]),
        ],
        'enabled' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable %l', ['%l' => $bundle->getLabel()]),
          '#default_value' => $settings->get($id) ? $settings->get($id)['enabled'] : 0,
        ],
        'icon_container' => [
          '#type' => 'details',
          '#attributes' => ['class' => ['icon-list', 'container-inline']],
          '#title' => $this->t('Icons'),
          '#open' => FALSE,
          'icons' => [],
        ],
      ];
      foreach ($bundle->getIcons() as $icon) {
        $form['bundles'][$id]['icon_container']['icons'][] = [
          '#type' => 'icon',
          '#bundle' => $id,
          '#icon' => $icon->getName(),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = \Drupal::configFactory()->getEditable('icon_bundles.settings');
    $previous_settings = $settings->getRawData();
    // Clear existing settings (to remove any old/missing bundles).
    $settings->setData([]);

    $bundles = $form_state->getValue('bundles', []);
    foreach ($bundles as $id => $values) {
      if (
        $values['enabled'] === 0
        && (isset($previous_settings[$id]['enabled']) && $previous_settings[$id]['enabled'] === 1)
      ) {
        // Disabling bundle that is current enabled.
        $field_config = \Drupal::service('entity_type.manager')->getStorage('field_config')->loadByProperties([
          'field_type' => 'icon',
        ]);
        /** @var \Drupal\field\Entity\FieldConfig $field */
        foreach ($field_config as $field) {
          // Check field config if using bundle.
          $field_bundles = $field->getSetting('bundles');
          // Remove bundle from field config.
          if (isset($field_bundles[$id])) {
            // Remove default value if needed.
            $default = $field->getDefaultValueLiteral();
            foreach ($default as $i => $value) {
              if ($value['bundle'] === $id) {
                unset($default[$i]);
              }
            }
            $field->setDefaultValue($default);
            unset($field_bundles[$id]);
            $field->setSetting('bundles', $field_bundles);
            $field->save();
          }
        }
      }
      $settings->set($id, $values);
    }

    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
